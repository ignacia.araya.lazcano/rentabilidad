#Solicitud de los datos "precio suscripcion", "numero usuarios", "gasto total"
p_suscripcion = int (input("Ingrese precio de suscripción:"))
n_usuarios = int(input("Ingrese número de usuarios:"))
gasto_total = int(input("Ingrese gasto total:"))
utilidades_anterior = int(input("Ingrese utilidades de año anterior: "))

#formula de Utilidades
utilidades = p_suscripcion * n_usuarios - gasto_total
razon_utilidades = utilidades / utilidades_anterior
print ("Sus utilidades son", utilidades)
print (f"La razón entre las utilidades actuales y del año anterior es {razon_utilidades:.2f}")